@echo off
setlocal
pushd %~dp0

set PATH=C:\jdk-23+37\bin;%PATH%

javac ^
-J-Duser.language=en ^
--enable-preview ^
--add-modules jdk.incubator.vector ^
--add-modules jdk.internal.vm.ci ^
--add-exports java.base/jdk.internal.vm=ALL-UNNAMED ^
--add-exports jdk.internal.vm.ci/jdk.vm.ci.code=ALL-UNNAMED ^
--add-exports jdk.internal.vm.ci/jdk.vm.ci.code.site=ALL-UNNAMED ^
--add-exports jdk.internal.vm.ci/jdk.vm.ci.hotspot=ALL-UNNAMED ^
--add-exports jdk.internal.vm.ci/jdk.vm.ci.meta=ALL-UNNAMED ^
--add-exports jdk.internal.vm.ci/jdk.vm.ci.runtime=ALL-UNNAMED ^
-d build ^
-source 23 ^
-cp lib\* ^
src\async\*.java ^
src\bench\*.java ^
src\llama\*.java ^
src\util\*.java ^
src\*.java

pause
