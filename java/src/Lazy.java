import java.util.function.Supplier;

enum Lazy {
	;

	public static <T> Supplier<T> lazy(Supplier<T> init) {
		return new Supplier<>() {
			private volatile T instance;

			@Override
			public T get() {
				T inst = instance;
				if (inst == null) {
					synchronized (this) {
						inst = instance;
						if (inst == null) {
							inst = init.get();
							if (inst == null)
								throw new NullPointerException();
							instance = inst;
						}
					}
				}
				return inst;
			}
		};
	}
}
