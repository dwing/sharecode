public class KongMingQi {
	private static final String[] MAP = {
			"  xxx  ", //        0  1  2
			"  xxx  ", //        3  4  5
			"xxxxxxx", //  6  7  8  9 10 11 12
			"xxxoxxx", // 13 14 15 16 17 18 19
			"xxxxxxx", // 20 21 22 23 24 25 26
			"  xxx  ", //       27 28 29
			"  xxx  ", //       30 31 32
	};
	private static final long[] MOVE_TABLE;
	private static final long INIT_MAP, TARGET_MAP;

	static {
		int w = MAP[0].length(), h = MAP.length, n = 0;
		int[] idxes = new int[w * h];
		for (int i = 0, y = 0; y < MAP.length; y++) {
			String line = MAP[y];
			for (int x = 0; x < line.length(); x++, i++)
				idxes[i] = line.charAt(x) != ' ' ? n++ : -1;
		}
		MOVE_TABLE = new long[n];
		long f = 0;
		for (int i = 0, j = 0, y = 0; y < MAP.length; y++) {
			String line = MAP[y];
			for (int x = 0; x < line.length(); x++, i++) {
				char c = line.charAt(x);
				if (c != ' ') {
					long i1, i2, m = 0;
					int k = 0;
					if (y > 1 && (i1 = idxes[i - w]) >= 0 && (i2 = idxes[i - w * 2]) >= 0) {
						m = (i1 << k) | (i2 << (k + 8));
						k = 16;
					}
					if (y < h - 2 && (i1 = idxes[i + w]) >= 0 && (i2 = idxes[i + w * 2]) >= 0) {
						m |= (i1 << k) | (i2 << (k + 8));
						k += 16;
					}
					if (x > 1 && (i1 = idxes[i - 1]) >= 0 && (i2 = idxes[i - 2]) >= 0) {
						m |= (i1 << k) | (i2 << (k + 8));
						k += 16;
					}
					if (x < w - 2 && (i1 = idxes[i + 1]) >= 0 && (i2 = idxes[i + 2]) >= 0)
						m |= (i1 << k) | (i2 << (k + 8));
					if (c == 'x')
						f |= 1L << j;
					MOVE_TABLE[j++] = m | (1L << 63);
				}
			}
		}
		INIT_MAP = f;
		TARGET_MAP = ~f & ((1L << n) - 1);
	}

	public static boolean search(long map) {
//		if (getAndSetFlag(map))
//			return;
		if (map == TARGET_MAP) {
//			result++;
			print(map);
			return true;
		}
		long m0, m1, m2;
		for (int i = 0, n = MOVE_TABLE.length; i < n; i++)
			if ((map & (m0 = 1L << i)) != 0)
				for (long m = MOVE_TABLE[i]; m != -1; m >>= 16)
					if ((map & (m1 = 1L << m)) != 0 && (map & (m2 = 1L << (m >> 8))) == 0)
						if (search(map & ~(m0 | m1) | m2)) {
							print(map);
							return true;
						}
		return false;
	}

	public static void main(String[] args) {
		search(INIT_MAP);
	}

	public static void print(long map) {
		for (int i = 0, y = 0; y < MAP.length; y++) {
			String line = MAP[y];
			for (int x = 0; x < line.length(); x++)
				System.out.print(line.charAt(x) == ' ' ? ' ' : ((map >> i++) & 1) == 0 ? '-' : 'x');
			System.out.println();
		}
		System.out.println("~~~~~~~~");
	}

/*
	private static final byte[] FLAGS = new byte[1 << (MOVE.length - 3)];

	private static boolean getAndSetFlag(long map) {
		int i = (int)(map >> 3);
		int j = 1 << (map & 7);
		int f = FLAGS[i];
		if ((f & j) != 0)
			return true;
		FLAGS[i] = (byte)(f | j);
//		flagCount++;
		return false;
	}

	private static int result;
	private static long flagCount;

	public static void main2(String[] args) {
		Thread t = new Thread(() -> {
			try {
				for (int i = 1; ; i++) {
					//noinspection BusyWait
					Thread.sleep(1000);
					System.out.format("result = %d, %ds, flags: %,d%n", result, i, flagCount);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		});
		t.setDaemon(true);
		t.start();
		search(INIT_MAP);
		System.out.format("result = %d, flags: %,d%n", result, flagCount);
	}
*/
}
