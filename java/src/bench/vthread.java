package bench;

import java.util.ArrayList;

public class vthread {
	public static void main(String[] args) throws Exception {
		var threads = new ArrayList<Thread>(100_000);
		for (int i = 0; i < 100_000; i++) {
			threads.add(Thread.startVirtualThread(() -> {
				try {
					for (int j = 0; j < 100; j++)
						Thread.sleep(100);
				} catch (Exception e) {
					throw new Error(e);
				}
			}));
		}
		for (var thread : threads)
			thread.join();
	}
}
