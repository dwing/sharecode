package bench;

public class fib {
	static int f(int n) {
		return n <= 2 ? 1 : f(n - 1) + f(n - 2);
	}

	public static void main(String[] args) {
		System.out.println(f(Integer.parseInt(args[0])));
	}
}
