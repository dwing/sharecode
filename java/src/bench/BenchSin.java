package bench;

import java.lang.foreign.Arena;
import java.lang.foreign.FunctionDescriptor;
import java.lang.foreign.Linker;
import java.lang.foreign.SymbolLookup;
import java.lang.invoke.MethodHandle;
import static java.lang.foreign.ValueLayout.JAVA_DOUBLE;

public class BenchSin {
	static final MethodHandle mhSin;

	static {
		var lib = SymbolLookup.libraryLookup("ucrtbase.dll", Arena.global());
		mhSin = Linker.nativeLinker().downcallHandle(lib.find("sin").orElseThrow(),
				FunctionDescriptor.of(JAVA_DOUBLE, JAVA_DOUBLE));
	}

	static void benchSinDll() throws Throwable {
		var t = System.nanoTime();
		var a = 0.0;
		for (int i = 0; i < 100000000; i++)
			a = (double)mhSin.invokeExact(a + i);
		System.out.println((System.nanoTime() - t) / 1_000_000 + " ms, " + a);
	}

	static void benchSin() {
		var t = System.nanoTime();
		var a = 0.0;
		for (int i = 0; i < 100000000; i++)
			a = Math.sin(a + i);
		System.out.println((System.nanoTime() - t) / 1_000_000 + " ms, " + a);
	}

	public static void main(String[] args) throws Throwable {
		for (int i = 0; i < 5; i++)
			benchSin();
	}
}
