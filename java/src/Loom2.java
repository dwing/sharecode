import java.util.concurrent.Executor;
import jdk.internal.vm.Continuation;
import jdk.internal.vm.ContinuationScope;
import util.UnsafeUtil;

// java --add-opens java.base/jdk.internal.vm=ALL-UNNAMED Loom2
public class Loom2 {
	public static void main(String[] args) throws Exception {
		var tb = Thread.ofVirtual();
		UnsafeUtil.getField(tb.getClass(), "scheduler").set(tb, (Executor)Runnable::run);

		var cs = new ContinuationScope("VirtualThreads");
		var c = new Continuation(cs, () -> {
			System.out.println("in Continuation: begin " + Thread.currentThread());
			Continuation.yield(cs);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			System.out.println("in Continuation: end " + Thread.currentThread());
		});
		System.out.println("main: begin " + Thread.currentThread());
		c.run();
		System.out.println("main: continue " + Thread.currentThread());
		c.run();
		System.out.println("main: end " + Thread.currentThread());
	}
}
