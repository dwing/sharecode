import java.util.function.IntBinaryOperator;

// -XX:+CriticalJNINatives (obsolete, only for JDK17-, see https://bugs.openjdk.org/browse/JDK-8289302)
// java -cp out TestJNI
public class TestJNI {
	public static native int nativeAdd(int a, int b);

	public static native int nativeAddBack(int a, int b, IntBinaryOperator cb);

	static {
		System.loadLibrary("add"); // need add.dll
	}

	static final int LOOP_COUNT = 1_0000_0000;

	static void testJNI() {
		long t = System.nanoTime();
		int n = 0;
		for (int i = 0; i < LOOP_COUNT; i++)
			n += nativeAdd(n, i);
		t = (System.nanoTime() - t) / 1_000_000;
		System.out.println("testJNI_Direct  : " + n + ", " + t + " ms");

		t = System.nanoTime();
		n = 0;
		for (int i = 0; i < LOOP_COUNT; i++)
			n += nativeAddBack(n, i, Integer::sum);
		t = (System.nanoTime() - t) / 1_000_000;
		System.out.println("testJNI_CallBack: " + n + ", " + t + " ms");
	}

	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			System.out.println("-------- " + i);
			testJNI();
		}
	}
}
