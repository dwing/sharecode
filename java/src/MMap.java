import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class MMap {
	public static void main(String[] args) throws Exception {
		ByteBuffer bb;
		try (var raf = new RandomAccessFile("test.txt", "r"); var fc = raf.getChannel()) {
			bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
		}
		Thread.sleep(30_000);
		System.out.println(bb.get());
	}
}
