import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.function.LongConsumer;

public class Test1 {
	public static Runnable foobar() throws Throwable {
		return (Runnable)LambdaMetafactory.metafactory(
				MethodHandles.lookup(),
				"run",
				MethodType.methodType(Runnable.class),
				MethodType.methodType(void.class),
				MethodHandles.lookup().findStatic(Test1.class, "lambda$foobar$0", MethodType.methodType(void.class)),
				MethodType.methodType(void.class)
		).dynamicInvoker().invoke();
	}

	private static void lambda$foobar$0() {
		System.out.println("Hello");
	}

	public static void main(String[] args) throws Throwable {
		LongConsumer c = v -> {
			System.out.println(v);
			System.out.println(args.length);
		};

		c.accept(123);

		foobar().run();
	}
}
