import java.security.SecureRandom;

class Xoshiro {
	private long s0, s1, s2, s3;

	public Xoshiro() {
		SecureRandom sr = new SecureRandom();
		do {
			s0 = sr.nextLong();
			s1 = sr.nextLong();
			s2 = sr.nextLong();
			s3 = sr.nextLong();
		}
		while ((s0 | s1 | s2 | s3) == 0);
	}

	public long nextLong() {
		long r = Long.rotateLeft(s1 * 5, 7) * 9;
		long n = s1 << 17;
		s2 ^= s0;
		s3 ^= s1;
		s1 ^= s2;
		s0 ^= s3;
		s2 ^= n;
		s3 = Long.rotateLeft(s3, 45);
		return r;
	}

	public int nextInt() {
		return (int)nextLong();
	}

	public float nextFloat() {
		return (float)(nextLong() >>> 40) * 0x1.0p-24f;
	}

	public double nextDouble() {
		return (double)(nextLong() >>> 11) * 0x1.0p-53;
	}
}

public class Test16 {
	public static void main(String[] args) {
		long t1 = System.currentTimeMillis();
		S s = new S();
		//noinspection UnnecessaryLocalVariable
		I i = s;
		var random = new Xoshiro(); // ThreadLocalRandom.current();
		for (long j = 0; j < 10_0000_0000; j++)
			i.f((int)(random.nextDouble() * 1000));
		System.out.println(((System.currentTimeMillis() - t1) / 1000f) + " sec");
		System.out.println(s.a);
	}

	interface I {
		void f(int d);
	}

	static class S implements I {
		long a;

		@Override
		public void f(int d) {
			a += d;
		}
	}
}
