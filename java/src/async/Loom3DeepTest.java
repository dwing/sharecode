package async;

import jdk.internal.vm.Continuation;
import jdk.internal.vm.ContinuationScope;

// java --add-opens java.base/jdk.internal.vm=ALL-UNNAMED -Xlog:gc=info,gc+heap=info:gc_loom.log:time async.Loom3DeepTest
// 1407 ms, r = 3315906880
public class Loom3DeepTest {
	private static final int N = 1000_0000;
	private static int s_i;
	private static final ContinuationScope cs = new ContinuationScope("VirtualThreads");

	private static void f(long v, int a) {
		if (v == 0) {
			s_i = a;
			Continuation.yield(cs);
		} else
			f(v >> 8, a + (int)(v & 0xff));
	}

	private static void f() {
		for (int i = 0; i < N; i++)
			f(0x100_0000_0000_0000L + i, 0);
	}

	public static void main(String[] args) {
		long t = System.nanoTime();
		var c = new Continuation(cs, Loom3DeepTest::f);
		long r = 0;
		for (int i = 0; i < N; ++i) {
			c.run();
			r += s_i;
		}
		System.out.println((System.nanoTime() - t) / 1_000_000 + " ms, r = " + r);
	}
}
