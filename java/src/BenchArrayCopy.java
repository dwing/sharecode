import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BenchArrayCopy {
	private static final int TEST_COUNT = 100;
	private static final int ARRAY_SIZE = 1024 * 1024 * 1024;
	private static final byte[] array0 = new byte[ARRAY_SIZE];
	private static final byte[] array1 = new byte[ARRAY_SIZE];
	private static final byte[] array2 = new byte[ARRAY_SIZE];
	private static final byte[] array3 = new byte[ARRAY_SIZE];

	private static void test0() {
		var a0 = array0;
		var a1 = array1;
		var t = System.nanoTime();
		for (int i = 0; i < TEST_COUNT; i++) {
			System.arraycopy(a0, 0, a1, 0, ARRAY_SIZE);
			var a = a0;
			a0 = a1;
			a1 = a;
		}
		System.out.println(System.nanoTime() - t);
	}

	private static void test1() {
		var a0 = array2;
		var a1 = array3;
		var t = System.nanoTime();
		for (int i = 0; i < TEST_COUNT; i++) {
			System.arraycopy(a0, 0, a1, 0, ARRAY_SIZE);
			var a = a0;
			a0 = a1;
			a1 = a;
		}
		System.out.println(System.nanoTime() - t);
	}

	public static void main(String[] args) throws InterruptedException {
		var es = Executors.newFixedThreadPool(2);
		for (int i = 0; i < 1; i++) {
			es.execute(BenchArrayCopy::test0);
			es.execute(BenchArrayCopy::test1);
		}
		es.awaitTermination(100, TimeUnit.SECONDS);
	}
}
