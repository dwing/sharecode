import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.HashSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class LiveDown {
	private static final int SLEEP_MIN = 5_000;
	private static final int SLEEP_MAX_COUNT = 6;
	private static final ThreadPoolExecutor tpe = (ThreadPoolExecutor)Executors.newFixedThreadPool(5);
	private static final HttpClient hc = HttpClient.newBuilder()
			.version(HttpClient.Version.HTTP_1_1)
			.followRedirects(HttpClient.Redirect.ALWAYS)
			.connectTimeout(Duration.ofSeconds(5))
			.build();

	private static void downloadFile(String url, Path file) {
		System.err.format("INFO : BEGIN:                             %s%n", url);
		for (int i = 1, RETRY = 2; ; i++) {
			var timeBegin = System.nanoTime();
			HttpResponse<Path> resp = null;
			String err = "";
			try {
				resp = hc.send(HttpRequest.newBuilder().uri(URI.create(url)).setHeader("User-Agent", "Mozilla/5.0")
						.build(), HttpResponse.BodyHandlers.ofFile(file, StandardOpenOption.CREATE,
						StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));
			} catch (Exception e) {
				var os = new ByteArrayOutputStream(256);
				e.printStackTrace(new PrintStream(os));
				err = os.toString();
			}
			var size = file.toFile().length();
			var time = (System.nanoTime() - timeBegin) * 1e-9;
			System.err.format("%s(%3d,%6dK,%5dK/s,%4.1fs): %s%n%s",
					err.isEmpty() ? "INFO : DONE " : i < RETRY ? "INFO : RETRY" : "ERROR: DONE ",
					resp != null ? resp.statusCode() : -1, size >>> 10, (long)(size / time) >>> 10, time,
					resp != null ? resp.uri() : url, err);
			if (err.isEmpty() || i >= RETRY)
				return;
			try {
				//noinspection BusyWait
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private static String downloadStr(String url) {
		System.err.format("INFO : BEGIN:                             %s%n", url);
		for (int i = 1, RETRY = 100; ; i++) {
			var timeBegin = System.nanoTime();
			HttpResponse<byte[]> resp = null;
			String err = "";
			try {
				resp = hc.sendAsync(HttpRequest.newBuilder().uri(URI.create(url)).setHeader("User-Agent", "Mozilla/5.0")
						.build(), HttpResponse.BodyHandlers.ofByteArray()).get(5, TimeUnit.SECONDS);
			} catch (Exception e) {
				var os = new ByteArrayOutputStream(256);
				e.printStackTrace(new PrintStream(os));
				err = os.toString();
			}
			var data = resp != null ? resp.body() : null;
			var size = data != null ? data.length : 0;
			var time = (System.nanoTime() - timeBegin) * 1e-9;
			System.err.format("%s(%3d,%6dK,%5dK/s,%4.1fs): %s%n%s",
					err.isEmpty() ? "INFO : DONE " : i < RETRY ? "INFO : RETRY" : "ERROR: DONE ",
					resp != null ? resp.statusCode() : -1, size >>> 10, (long)(size / time) >>> 10, time,
					resp != null ? resp.uri() : url, err);
			if (err.isEmpty() || i >= RETRY)
				return data != null ? new String(data, StandardCharsets.UTF_8) : "";
			try {
				//noinspection BusyWait
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	static final boolean[] fileNameChars = new boolean[128];

	static {
		for (int i = '0'; i <= '9'; i++)
			fileNameChars[i] = true;
		for (int i = 'A'; i <= 'Z'; i++)
			fileNameChars[i] = true;
		for (int i = 'a'; i <= 'z'; i++)
			fileNameChars[i] = true;
		var allowedChars = "._-+~!@#$()[]{}";
		for (int i = 0; i < allowedChars.length(); i++)
			fileNameChars[allowedChars.charAt(i)] = true;
	}

	public static int lastIndexOfFileNameBegin(String s) {
		int i = s.length();
		while (--i >= 0) {
			int c = s.charAt(i);
			if (c >= 128 || !fileNameChars[c])
				break;
		}
		return i + 1;
	}

	public static int countDigits(String s) {
		int r = 0;
		for (int i = 0, n = s.length(); i < n; i++) {
			int c = s.charAt(i);
			if (c >= '0' && c <= '9')
				r++;
		}
		return r;
	}

	public static void main(String[] args) throws Exception {
		// http://37.27.111.214:8080/live/20/hls.m3u8
		// https://bsex.serv00.net/Smart.php?id=ettvnews
		var url = args[0]; // m3u8 url
		var urlPath = url.substring(0, url.lastIndexOf('/') + 1);
		var outPath = args.length > 1 ? args[1] : ".";
		var fileNameSet = new HashSet<String>();
		//noinspection InfiniteLoopStatement
		for (var idx = new int[1]; ; ) {
			try {
				downloadStr(url).lines().forEach(line -> {
					if (!line.isEmpty() && line.charAt(0) != '#') {
						String fileUrl, fileName;
						if (line.contains("://")) {
							fileUrl = line;
							fileName = line.substring(lastIndexOfFileNameBegin(line));
						} else {
							fileUrl = urlPath + line;
							fileName = line;
						}
						if (!fileName.isEmpty()) {
							fileName = fileName.replace('/', '_');
							if (fileName.indexOf('.') < 0)
								fileName += ".ts";
							if (countDigits(fileName) < 6)
								fileName = String.format("%6d_%s", ++idx[0], fileName);
							if (fileNameSet.add(fileName)) {
								var filePath = Path.of(outPath, fileName);
								tpe.execute(() -> downloadFile(fileUrl, filePath));
							}
						}
					}
				});
			} catch (Exception e) {
				//noinspection CallToPrintStackTrace
				e.printStackTrace();
			}
			for (int i = 0; i < SLEEP_MAX_COUNT; i++) {
				//noinspection BusyWait
				Thread.sleep(SLEEP_MIN);
				if (tpe.getQueue().isEmpty())
					break;
			}
			System.err.format("INFO : QUEUE: %d => %d/%d => %d%n",
					tpe.getQueue().size(), tpe.getActiveCount(), tpe.getCorePoolSize(), tpe.getCompletedTaskCount());
		}
	}
}
