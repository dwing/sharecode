import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

public class TestSwing extends JFrame {
	public final Canvas canvas;
	public final BufferedImage img;
	public final int[] imgData;

	public TestSwing() {
		super("Swing测试");

//		var panel = (JPanel)getContentPane();
//		panel.setPreferredSize(new Dimension(512, 512));
//		panel.setBackground(Color.GRAY);
//		panel.setLayout(null);

		img = new BufferedImage(300, 300, BufferedImage.TYPE_INT_RGB);
		imgData = ((DataBufferInt)img.getRaster().getDataBuffer()).getData();

		var menuBar = new JMenuBar();
		var menu = menuBar.add(new JMenu("文件(File)"));
		menu.add(new JMenuItem("新建(New)", KeyEvent.VK_N)).addActionListener(System.out::println);
		setJMenuBar(menuBar);

		var menuPop = new JPopupMenu();
		menuPop.add(new JMenuItem("关于(About)", KeyEvent.VK_A)).addActionListener(System.out::println);
		add(menuPop);

		add(canvas = new Canvas() {
			{
				setSize(512, 512);
				setBackground(Color.GRAY);
				setFocusable(true);

				addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						var p1 = MouseInfo.getPointerInfo().getLocation();
						var p0 = getLocationOnScreen();
						int x = p1.x - p0.x;
						int y = p1.y - p0.y;
						System.out.println(e + ", " + x + ", " + y);
						repaint();
					}
				});
				addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						System.out.println(e);
						if (e.isPopupTrigger())
							menuPop.show(canvas, e.getX(), e.getY());
					}
				});
				addMouseMotionListener(new MouseMotionAdapter() {
					@Override
					public void mouseDragged(MouseEvent e) {
						System.out.println(e);
						repaint();
					}
				});
				addMouseWheelListener(System.out::println);
			}

			@Override
			public void update(Graphics g) {
				System.out.println("update: " + g.getClass().getName());
				for (int i = 0; i < 10; i++)
					imgData[ThreadLocalRandom.current().nextInt(300 * 300)] = -1;
				// g.clearRect(0, 0, getWidth(), getHeight());
				paint(g);
			}

			@Override
			public void paint(Graphics g) {
				g.drawImage(img, 0, 0, null);
			}
		});

		setResizable(true);
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				System.out.println("resize: " + e);
			}
		});

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setLocationByPlatform(true);
		pack();
		setVisible(true);
	}

	public static void main(String[] args) {
		System.setProperty("sun.java2d.uiScale.enabled", "false");
		System.setProperty("sun.awt.noerasebackground", "true");
		// System.setProperty("sun.java2d.uiScale", "2x");
		// System.setProperty("sun.java2d.opengl", "true");
		// System.setProperty("sun.java2d.noddraw", "true");
		new TestSwing();
	}
}
