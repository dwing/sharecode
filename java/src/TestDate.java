public class TestDate {
	private static final int[] DAYS = new int[]{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
//	private static final long DAYS_MAGIC0, DAYS_MAGIC1;

	static {
		int n = 0;
//		long m0 = 0, m1 = 0;
		for (int i = 0; i < DAYS.length; i++) {
			n += DAYS[i];
			DAYS[i] = n;
//			if (i <= 6)
//				m0 += (long)n << (i * 9);
//			if (i >= 6)
//				m1 += (long)n << ((i - 6) * 9);
		}
//		DAYS_MAGIC0 = m0;
//		DAYS_MAGIC1 = m1;
	}

	// [0~365] => [1~12]*32 + [1~31]
	public static int toMonthDay1(int days, int leap) {
		days += (1 - leap) & ((58 - days) >>> 31);
		for (int i = 1; i < DAYS.length; i++) {
			if (days < DAYS[i])
				return i * 32 + days - DAYS[i - 1] + 1;
		}
		return 0;
	}

	// [0~365] => [1~12]*32 + [1~31]
	public static int toMonthDay2(int days, int leap) {
		days += (1 - leap) & ((58 - days) >>> 31);
		int i = days >> 5;
		if (days < DAYS[i + 1])
			return (i + 1) * 32 + days - DAYS[i] + 1;
		return (i + 2) * 32 + days - DAYS[i + 1] + 1;
	}

	// [0~365] => [1~12]*32 + [1~31]
	public static int toMonthDay3(int days, int leap) {
		days += (1 - leap) & ((58 - days) >>> 31);
		int i = days >> 5;
		int j = i;
		var m = 3283976880569466368L; // DAYS_MAGIC0
		if (i >= 6) {
			j = i - 6;
			m = 6605077615400299190L; // DAYS_MAGIC1
		}
		int n = (int)(m >> (j * 9));
		int d = days - ((n >> 9) & 511);
		if (d < 0) {
			d = days - (n & 511);
			i--;
		}
		return (i + 2) * 32 + d + 1;
	}

	public static void main(String[] args) {
		for (int i = 0, n = 365; i < n; i++) {
			var md1 = toMonthDay1(i, 0);
			var md2 = toMonthDay2(i, 0);
			var md3 = toMonthDay3(i, 0);
			if (md1 != md2 || md1 != md3)
				throw new AssertionError(i);
			System.out.println((md1 >> 5) + "-" + (md1 & 31));
		}
		for (int i = 0, n = 366; i < n; i++) {
			var md1 = toMonthDay1(i, 1);
			var md2 = toMonthDay2(i, 1);
			var md3 = toMonthDay3(i, 1);
			if (md1 != md2 || md1 != md3)
				throw new AssertionError(i);
			System.out.println((md1 >> 5) + "-" + (md1 & 31));
		}
//		System.out.println(DAYS_MAGIC0);
//		System.out.println(DAYS_MAGIC1);
	}
}
