import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.MouseInfo;
import java.awt.PopupMenu;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.concurrent.ThreadLocalRandom;

public class TestAwt extends Frame {
	public final Canvas canvas;
	public final BufferedImage img;
	public final byte[] imgData;
	// private Graphics backG;

	public TestAwt() {
		super("AWT测试");

		img = new BufferedImage(300, 300, BufferedImage.TYPE_3BYTE_BGR);
		imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();

		var menuBar = new MenuBar();
		var menu = menuBar.add(new Menu("文件(File)"));
		menu.add(new MenuItem("新建(New)", new MenuShortcut(KeyEvent.VK_N))).addActionListener(System.out::println);
		setMenuBar(menuBar);

		var menuPop = new PopupMenu();
		menuPop.add(new MenuItem("关于(About)", new MenuShortcut(KeyEvent.VK_A))).addActionListener(System.out::println);
		add(menuPop);

		add(canvas = new Canvas() {
			{
				setSize(512, 512);
				setBackground(Color.GRAY);
				setFocusable(true);

				addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						var p1 = MouseInfo.getPointerInfo().getLocation();
						var p0 = getLocationOnScreen();
						int x = p1.x - p0.x;
						int y = p1.y - p0.y;
						System.out.println(e + ", " + x + ", " + y);
						repaint();
					}
				});
				addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						System.out.println(e);
						if (e.isPopupTrigger())
							menuPop.show(canvas, e.getX(), e.getY());
					}
				});
				addMouseMotionListener(new MouseMotionAdapter() {
					@Override
					public void mouseDragged(MouseEvent e) {
						System.out.println(e);
						repaint();
					}
				});
				addMouseWheelListener(System.out::println);
			}

			@Override
			public void update(Graphics g) {
				System.out.println("update: " + g.getClass().getName());
				for (int i = 0; i < 10; i++)
					imgData[ThreadLocalRandom.current().nextInt(300 * 300 * 3)] = (byte)0xff;
				paint(g);
			}

			@Override
			public void paint(Graphics g) {
//				g = backG;
//				if (g == null)
//					createBufferStrategy(2);
//				if (g == null || getBufferStrategy().contentsLost()) {
//					backG = g = getBufferStrategy().getDrawGraphics();
//					g.setFont(new Font(null, Font.PLAIN, 20));
//				}
				g.drawImage(img, 0, 0, null);
//				getBufferStrategy().show();
			}
		});

		setResizable(true);
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				System.out.println("resize: " + e);
			}
		});

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		setLocationByPlatform(true);
		pack();
		setVisible(true);
	}

	// -Dfile.encoding=gbk
	public static void main(String[] args) {
		System.setProperty("sun.java2d.uiScale.enabled", "false");
		System.setProperty("sun.awt.noerasebackground", "true");
		// System.setProperty("sun.java2d.uiScale", "2x");
		// System.setProperty("sun.java2d.opengl", "true");
		new TestAwt();
	}
}
