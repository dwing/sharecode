using System;
using System.Runtime.CompilerServices;

class Bench1 {
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	static long fib(long n) {
		return n <= 2 ? 1 : fib(n - 1) + fib(n - 2);
	}

	static void Main(string[] args) {
		Console.WriteLine(fib(45));
	}
}
