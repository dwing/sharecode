using System;

class Bench2 {
	interface I {
		void f();
	}

	class S : I {
		public long a;

		public void f() {
			a++;
		}
	}

	static void Main(string[] args) {
		S s = new S();
		I i = s;
		for(long j = 0; j < 10_0000_0000; j++)
			i.f();
		Console.WriteLine(s.a);
	}
}
