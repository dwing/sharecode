using System;
using System.Diagnostics;

Stopwatch sw = Stopwatch.StartNew();

S s = new S();
I i = s;
Random rnd = new Random();
for (long j = 0; j < 10_0000_0000; j++)
{
	i.f((int)(rnd.NextDouble() * 1000));
}

Console.WriteLine($"耗时：{sw.Elapsed.TotalSeconds:0.0000}");
Console.WriteLine(s.a);

Console.ReadLine();

interface I
{
	void f(int d);
}

class S : I
{
	public long a;

	public void f(int d)
	{
		a += d;
	}
}
