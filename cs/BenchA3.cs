using System;

class Bench3 {
	class E {
		public int a;

		public E(int a) {
			this.a = a;
		}
	}

	static void Main(string[] args) {
		const long ARRAY_COUNT = 10000L;
		const long TEST_COUNT = ARRAY_COUNT * 10_0000L;

		E[] es = new E[(int)ARRAY_COUNT];
		for(long i = 0; i < TEST_COUNT; i++)
			es[(int)(i * 123456789L % ARRAY_COUNT)] = new E((int)i);

		long n = 0;
		for(long i = 0; i < ARRAY_COUNT; i++) {
			E e = es[(int)i];
			if(e != null)
				n += e.a;
		}
		Console.WriteLine(n);
	}
}
