using System;

class Bench2a {
	interface I {
		void f();
		long get();
	}

	class S1 : I {
		long a;

		public void f() {
			a++;
		}
		
		public long get() {
			return a;
		}
	}

	class S2 : I {
		long a;

		public void f() {
			a--;
		}
		
		public long get() {
			return a;
		}
	}

	static void Main(string[] args) {
		I[] ia = new I[] { new S1(), new S2(), new S1() };
		for(long j = 0; j < 10_0000_0000; j++)
			ia[(int)(j % 3)].f();
		Console.WriteLine(ia[0].get() + ia[1].get() + ia[2].get());
	}
}
