using System;

class Bench4 {
	static void Main(string[] args) {
		long a = 0;
		Action[] ia = new Action[] {
			() => a++,
			() => a--,
			() => a++,
		};
		for(long j = 0; j < 10_0000_0000; j++)
			ia[(int)(j % 3)]();
		Console.WriteLine(a);
	}
}
