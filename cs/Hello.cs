using System.Runtime.InteropServices;

public static class Hello
{
	[UnmanagedCallersOnly]
	private static unsafe void Entry()
	{
		System.Console.WriteLine("Hello, World");
	}

	[UnmanagedCallersOnly]
	private static unsafe int Add(int a, int b)
	{
		return a + b;
	}

	public static void Main()
	{
	}
}
