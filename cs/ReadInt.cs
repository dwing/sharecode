public static class ReadInt
{
	const int TestCount = 1000;

	static int SumSafe(byte[] data)
	{
		var r = 0;
		for (var j = 0; j < TestCount; j++)
		{
			for (int i = 0, n = data.Length - 4; i < n; i++)
				r += data[i] + (data[i + 1] << 8) + (data[i + 2] << 16) + (data[i + 3] << 24);
		}
		return r;
	}

	static unsafe int SumUnsafe(byte[] data)
	{
		var r = 0;
		for (var j = 0; j < TestCount; j++)
		{
			for (int i = 0, n = data.Length - 4; i < n; i++)
				fixed (byte* p = &data[i])
					r += *(int*)p;
		}
		return r;
	}

	static void Test(byte[] data)
	{
		var sw = System.Diagnostics.Stopwatch.StartNew();
		var r = SumSafe(data);
		sw.Stop();
		System.Console.WriteLine("SumSafe  : " + r + ", time: " + sw.Elapsed);

		sw = System.Diagnostics.Stopwatch.StartNew();
		r = SumUnsafe(data);
		sw.Stop();
		System.Console.WriteLine("SumUnsafe: " + r + ", time: " + sw.Elapsed);
	}

	public static void Main()
	{
		var data = new byte[1 << 20];
		new System.Random().NextBytes(data);

		Test(data);
		Test(data);
		Test(data);
	}
}
