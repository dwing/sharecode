@echo off
setlocal

set DOTNETCORE_HOME=C:\Program Files\dotnet
set DOTNETCORE_SDK=%DOTNETCORE_HOME%\sdk\6.0.301
set DOTNETCORE_SHARED=%DOTNETCORE_HOME%\shared\Microsoft.NETCore.App\6.0.6
set PATH=%DOTNETCORE_HOME%;%PATH%

dotnet "%DOTNETCORE_SDK%\Roslyn\bincore\csc.dll" /target:exe /unsafe+ /optimize+ /debug- /r:"%DOTNETCORE_SDK%\ref\netstandard.dll" %*

rem dotnet.exe "%DOTNETCORE_SDK%\Roslyn\bincore\csc.dll" /target:exe /unsafe+ /optimize+ /debug- ^
rem /r:"%DOTNETCORE_SHARED%\System.Private.CoreLib.dll" ^
rem /r:"%DOTNETCORE_SHARED%\System.Console.dll" ^
rem /r:"%DOTNETCORE_SHARED%\System.Runtime.dll" ^
rem /r:"%DOTNETCORE_SHARED%\System.Runtime.InteropServices.RuntimeInformation.dll" ^
rem /r:"%DOTNETCORE_SHARED%\System.Threading.Channels.dll" ^
rem /r:"%DOTNETCORE_SHARED%\System.Collections.dll" ^
rem /r:"%DOTNETCORE_SHARED%\System.Linq.dll" ^
rem %*
