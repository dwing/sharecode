using System;

public class FFT
{
	private const int                N   = 1024 * 1024;
	private static readonly double[] SIN = new double[N];
	private static readonly double[] COS = new double[N];

	public static int highestOneBit(int i)
	{
		i |= (i >>  1);
		i |= (i >>  2);
		i |= (i >>  4);
		i |= (i >>  8);
		i |= (i >>  16);
		return i - (i >> 1);
	}

	static FFT()
	{
		if (highestOneBit(N) != N)
			throw new ArgumentException("N is not a power of 2");
		for (int i = 0; i < N; i++)
		{
			double kth = i * -Math.PI / N;
			SIN[i] = Math.Sin(kth);
			COS[i] = Math.Cos(kth);
		}
	}

	private static void mult(double[] a, int k, double r, double i)
	{
		double ar = a[k], ai = a[k + 1];
		a[k] = ar * r - ai * i;
		a[k + 1] = ar * i + ai * r;
	}

	private static double[] fft(double[] x, int offset, int stride, int count)
	{
		if (count == 2)
			return new double[] { x[offset], x[offset + 1] };
		double[] q = fft(x, offset, stride * 2, count >> 1);
		double[] r = fft(x, offset + stride, stride * 2, count >> 1);
		double[] y = new double[count];
		count >>= 1;
		for (int i = 0, j = N * 2 / count, k = 0; k < count; k++, i += j)
		{
			mult(r, k, COS[i], SIN[i]);
			y[k] = q[k] + r[k];
			y[k + count] = q[k] - r[k];
			k++;
			y[k] = q[k] + r[k];
			y[k + count] = q[k] - r[k];
		}
		return y;
	}

	public static double[] fft(double[] x)
	{
		if (highestOneBit(x.Length) != x.Length)
			throw new ArgumentException("x.Length is not a power of 2");
		return fft(x, 0, 2, x.Length);
	}

	public static double[] ifft(double[] x)
	{
		if (highestOneBit(x.Length) != x.Length)
			throw new ArgumentException("x.Length is not a power of 2");
		int n = x.Length;
		for (int i = 1; i < n; i += 2)
			x[i] = -x[i];
		x = fft(x);
		double r = 2.0 / n;
		for (int i = 0; i < n; i += 2)
		{
			x[i] *= r;
			x[i + 1] *= -r;
		}
		return x;
	}

	public static double[] cconvolve(double[] x, double[] y)
	{
		int n = x.Length;
		if (n != y.Length)
			throw new ArgumentException("x.Length != y.Length");
		double[] a = fft(x);
		double[] b = fft(y);
		for (int i = 0; i < n; i += 2)
			mult(a, i, b[i], b[i + 1]);
		return ifft(a);
	}

	public static double[] convolve(double[] x, double[] y)
	{
		double[] xx = new double[x.Length * 2];
		double[] yy = new double[y.Length * 2];
		Array.Copy(x, xx, x.Length);
		Array.Copy(y, yy, y.Length);
		return cconvolve(xx, yy);
	}

	public static double warmup()
	{
		double[] x = new double[N * 2];
		for (int i = 0; i < N * 2; i += 2)
			x[i] = (double)i / -N + 1;
		double[] y = fft(x);
		double[] z = ifft(y);
		double[] c = cconvolve(x, x);
		double[] d = convolve(x, x);

		double r = 0;
		for (int i = 0; i < N * 2; i += 2)
			r += y[i] + z[i] + c[i] + d[i];
		return r;
	}

	public static void Main(string[] args)
	{
		for (int i = 0; i < 5; i++)
			warmup();
		for (int j = 0; j < 5; j++)
		{
			DateTime begintime = DateTime.Now;
			double r = warmup();
			System.Console.WriteLine((DateTime.Now - begintime).TotalMilliseconds);
			System.Console.WriteLine(r); // 输出结果,避免计算过程被编译器优化掉
		}
	}
}
