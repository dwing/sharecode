@echo off
setlocal
pushd %~dp0

set PATH=C:\jdk-16.0.1+9\bin;%PATH%

rem java -XX:+UseParallelGC -Xms400m -Xmx400m -cp kotlin-stdlib.jar;out Bench1Kt
java -XX:+UseParallelGC -Xms2g -Xmx2g -cp kotlin-stdlib.jar;out Bench2Kt

pause
