// A concurrent prime sieve from go officical example

package main

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
)

// Send the sequence 2, 3, 4, ... to channel 'ch'.
func Generate(ch chan<- int) {
	for i := 2; ; i++ {
		ch <- i // Send 'i' to channel 'ch'.
	}
}

// Copy the values from channel 'in' to channel 'out',
// removing those divisible by 'prime'.
func Filter(in <-chan int, out chan<- int, prime int) {
	for {
		i := <-in // Receive value from 'in'.
		if i%prime != 0 {
			out <- i // Send 'i' to 'out'.
		}
	}
}

// The prime sieve: Daisy-chain Filter processes.
func main() {
	if len(os.Args) > 2 {
		runtime.GOMAXPROCS(1)
	}
	n := 10000
	if len(os.Args) > 1 {
		if _n, err := strconv.Atoi(os.Args[1]); err == nil {
			n = _n
		}
	}
	ch := make(chan int) // Create a new channel.
	go Generate(ch)      // Launch Generate goroutine.
	i := 0
	for {
		prime := <-ch
		i++
		if i == n {
			fmt.Println(prime)
			os.Exit(0)
		}
		ch1 := make(chan int)
		go Filter(ch, ch1, prime)
		ch = ch1
	}
}
