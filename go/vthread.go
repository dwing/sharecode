package main

import (
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(100_000)
	for i := 0; i < 100_000; i++ {
		go func() {
			for j := 0; j < 100; j++ {
				time.Sleep(100 * time.Millisecond)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}
