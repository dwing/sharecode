package main

import (
	"fmt"
	"time"
)

type I interface {
	f()
}

type S struct {
	a int64
}

func (this *S) f() {
	this.a++
}

func testI() {
	t := time.Now().UnixNano()
	var s S
	var i I = &s
	for j := 0; j < 1000000000; j++ {
		i.f()
	}
	fmt.Printf("testI: r=%d, %d ms\n", s.a, (time.Now().UnixNano()-t)/1_000_000)
}

func testS() {
	t := time.Now().UnixNano()
	var s S
	for j := 0; j < 1000000000; j++ {
		s.f()
	}
	fmt.Printf("testS: r=%d, %d ms\n", s.a, (time.Now().UnixNano()-t)/1_000_000)
}

func main() {
	testI()
	testS()
}
