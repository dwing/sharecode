@echo off
setlocal
pushd %~dp0

cd ..\src

set VCBASE=C:\Programs\vc6

set PATH=%VCBASE%\VC100\bin;%PATH%
set INCLUDE=%VCBASE%\VC71\include;%VCBASE%\VC98\include
set LIB=%VCBASE%\VC71\lib;%VCBASE%\VC98\lib;..\!build

set LJCOMPILE=cl /nologo /c /DLUAJIT_ENABLE_LUA52COMPAT /D_CRT_SECURE_NO_DEPRECATE /D_ftelli64=ftell /D_fseeki64=fseek /DSTATUS_LONGJUMP=0x80000026 /I. /I..\dynasm /W3 /MD /O2 /Ob2 /fp:fast /GS- /GL
set LJLINK=link /nologo ftol2.obj ftol2a.obj BufferOverflowU.lib kernel32.lib /pdb:none /LTCG
set ALL_LIB=lib_base.c lib_math.c lib_bit.c lib_string.c lib_table.c lib_io.c lib_os.c lib_package.c lib_debug.c lib_jit.c lib_ffi.c lib_buffer.c

%LJCOMPILE% host\minilua.c
%LJLINK% /out:minilua32_vc.exe minilua.obj

minilua32_vc.exe ..\dynasm\dynasm.lua -LN -D WIN -D JIT -D FFI -D ENDIAN_LE -D FPU -o host\buildvm_arch.h vm_x86.dasc

git show -s --format=%%ct >luajit_relver.txt
minilua32_vc.exe host/genversion.lua

%LJCOMPILE% host\buildvm*.c
%LJLINK% /out:buildvm32_vc.exe buildvm*.obj

buildvm32_vc.exe -m peobj   -o lj_vm.obj
buildvm32_vc.exe -m bcdef   -o lj_bcdef.h    %ALL_LIB%
buildvm32_vc.exe -m ffdef   -o lj_ffdef.h    %ALL_LIB%
buildvm32_vc.exe -m libdef  -o lj_libdef.h   %ALL_LIB%
buildvm32_vc.exe -m recdef  -o lj_recdef.h   %ALL_LIB%
buildvm32_vc.exe -m vmdef   -o jit\vmdef.lua %ALL_LIB%
buildvm32_vc.exe -m folddef -o lj_folddef.h lj_opt_fold.c

%LJCOMPILE% /DLUA_BUILD_AS_DLL ljamalg.c
%LJLINK% /out:%~dp0\luajit32_vc.dll ljamalg.obj lj_vm.obj /dll /noentry /implib:%~dp0\luajit32_vc.lib
%LJCOMPILE% ljamalg.c luajit.c
%LJLINK% /out:%~dp0\luajit32_vc.exe ljamalg.obj lj_vm.obj luajit.obj /fixed

rem del buildvm*.obj minilua32_vc.exe buildvm32_vc.exe

pause
