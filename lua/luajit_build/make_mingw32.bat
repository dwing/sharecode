@echo off
setlocal
pushd %~dp0

cd ../src

set GCCEXE=C:/mingw/bin/gcc.exe
set DLLEXE=C:/mingw/bin/dlltool.exe

set LJCOMPILE=%GCCEXE% -pipe -static -Ofast -fweb -fomit-frame-pointer -fmerge-all-constants -s -Wall -Wextra -I. -I../dynasm -DNDEBUG -DLUAJIT_ENABLE_LUA52COMPAT
set ALL_LIB=lib_base.c lib_math.c lib_bit.c lib_string.c lib_table.c lib_io.c lib_os.c lib_package.c lib_debug.c lib_jit.c lib_ffi.c lib_buffer.c

%LJCOMPILE% -o minilua32.exe host/minilua.c

minilua32.exe ../dynasm/dynasm.lua -LN -D WIN -D JIT -D FFI -D ENDIAN_LE -D FPU -o host/buildvm_arch.h vm_x86.dasc

git show -s --format=%%ct >luajit_relver.txt
minilua32.exe host/genversion.lua

%LJCOMPILE% -m32 -o buildvm32.exe host/buildvm.c host/buildvm_asm.c host/buildvm_fold.c host/buildvm_lib.c host/buildvm_peobj.c

buildvm32.exe -m peobj   -o lj_vm.obj
buildvm32.exe -m bcdef   -o lj_bcdef.h    %ALL_LIB%
buildvm32.exe -m ffdef   -o lj_ffdef.h    %ALL_LIB%
buildvm32.exe -m libdef  -o lj_libdef.h   %ALL_LIB%
buildvm32.exe -m recdef  -o lj_recdef.h   %ALL_LIB%
buildvm32.exe -m vmdef   -o jit/vmdef.lua %ALL_LIB%
buildvm32.exe -m folddef -o lj_folddef.h lj_opt_fold.c

%LJCOMPILE% -m32 -Wl,--output-def,%~dp0/luajit32.def,--out-implib,%~dp0/luajit32.a -DLUA_BUILD_AS_DLL -shared -o %~dp0/luajit32.dll ljamalg.c lj_vm.obj
%DLLEXE% -d %~dp0/luajit32.def --dllname %~dp0/luajit32.dll --output-lib %~dp0/luajit32.lib --kill-at
%LJCOMPILE% -m32 -o %~dp0/luajit32.exe ljamalg.c luajit.c lj_vm.obj

rem del buildvm*.obj minilua32.exe buildvm32.exe

pause
