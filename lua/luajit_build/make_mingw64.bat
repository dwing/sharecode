@echo off
setlocal
pushd %~dp0

cd ../src

set GCCEXE=C:/mingw/bin/x86_64-w64-mingw32-gcc.exe

set LJCOMPILE=%GCCEXE% -pipe -static -Ofast -fweb -fomit-frame-pointer -fmerge-all-constants -s -Wall -Wextra -I. -I../dynasm -DNDEBUG -DLUAJIT_ENABLE_LUA52COMPAT -DLUAJIT_ENABLE_GC64 -DLUAJIT_TARGET=LUAJIT_ARCH_X64 -flto
set ALL_LIB=lib_base.c lib_math.c lib_bit.c lib_string.c lib_table.c lib_io.c lib_os.c lib_package.c lib_debug.c lib_jit.c lib_ffi.c lib_buffer.c

%LJCOMPILE% -o minilua64.exe host/minilua.c

minilua64.exe ../dynasm/dynasm.lua -LN -D WIN -D JIT -D FFI -D ENDIAN_LE -D FPU -D P64 -o host/buildvm_arch.h vm_x64.dasc

git show -s --format=%%ct >luajit_relver.txt
minilua64.exe host/genversion.lua

%LJCOMPILE% -m64 -o buildvm64.exe host/buildvm.c host/buildvm_asm.c host/buildvm_fold.c host/buildvm_lib.c host/buildvm_peobj.c

buildvm64.exe -m peobj   -o lj_vm.obj
buildvm64.exe -m bcdef   -o lj_bcdef.h    %ALL_LIB%
buildvm64.exe -m ffdef   -o lj_ffdef.h    %ALL_LIB%
buildvm64.exe -m libdef  -o lj_libdef.h   %ALL_LIB%
buildvm64.exe -m recdef  -o lj_recdef.h   %ALL_LIB%
buildvm64.exe -m vmdef   -o jit/vmdef.lua %ALL_LIB%
buildvm64.exe -m folddef -o lj_folddef.h lj_opt_fold.c

%LJCOMPILE% -m64 -Wl,--output-def,%~dp0/luajit64.def,--out-implib,%~dp0/luajit64.a -DLUA_BUILD_AS_DLL -shared -o %~dp0/luajit64.dll ljamalg.c lj_vm.obj

%LJCOMPILE% -m64 -o %~dp0/luajit64.exe ljamalg.c luajit.c lj_vm.obj

rem del buildvm*.obj minilua64.exe buildvm64.exe

pause
