# Microsoft Developer Studio Project File - Name="luajit" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=luajit - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "luajit.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "luajit.mak" CFG="luajit - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "luajit - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe
# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /O2 /Ob2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D WINVER=0x0500 /D _ftelli64=ftell /D _fseeki64=fseek /FD /GS- /GL /fp:fast /c
# ADD BASE RSC /l 0x804 /d "NDEBUG"
# ADD RSC /l 0x804 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib ftol2.obj ftol2a.obj BufferOverflowU.lib ..\src\lj_vm.obj /nologo /subsystem:console /pdb:none /machine:I386 /LTCG
# Begin Target

# Name "luajit - Win32 Release"
# Begin Source File

SOURCE=..\src\lauxlib.h
# End Source File
# Begin Source File

SOURCE=..\src\lib_aux.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_base.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_bit.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_debug.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_ffi.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_init.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_io.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_jit.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_math.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_os.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_package.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_string.c
# End Source File
# Begin Source File

SOURCE=..\src\lib_table.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_alloc.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_alloc.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_api.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_arch.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_asm.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_asm.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_asm_arm.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_asm_mips.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_asm_ppc.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_asm_x86.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_bc.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_bc.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_bcdef.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_bcdump.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_bcread.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_bcwrite.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_carith.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_carith.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ccall.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_ccall.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ccallback.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_ccallback.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_cconv.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_cconv.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_cdata.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_cdata.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_char.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_char.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_clib.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_clib.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_cparse.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_cparse.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_crecord.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_crecord.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ctype.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_ctype.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_debug.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_debug.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_def.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_dispatch.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_dispatch.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_emit_arm.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_emit_mips.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_emit_ppc.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_emit_x86.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_err.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_err.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_errmsg.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ff.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ffdef.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ffrecord.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_ffrecord.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_folddef.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_frame.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_func.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_func.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_gc.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_gc.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_gdbjit.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_gdbjit.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ir.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_ir.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_ircall.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_iropt.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_jit.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_lex.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_lex.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_lib.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_lib.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_libdef.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_mcode.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_mcode.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_meta.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_meta.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_obj.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_obj.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_opt_dce.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_opt_fold.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_opt_loop.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_opt_mem.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_opt_narrow.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_opt_sink.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_opt_split.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_parse.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_parse.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_recdef.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_record.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_record.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_snap.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_snap.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_state.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_state.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_str.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_str.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_tab.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_tab.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_target.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_target_arm.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_target_mips.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_target_ppc.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_target_x86.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_trace.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_trace.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_traceerr.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_udata.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_udata.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_vm.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_vmevent.c
# End Source File
# Begin Source File

SOURCE=..\src\lj_vmevent.h
# End Source File
# Begin Source File

SOURCE=..\src\lj_vmmath.c
# End Source File
# Begin Source File

SOURCE=..\src\ljamalg.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\src\lua.h
# End Source File
# Begin Source File

SOURCE=..\src\lua.hpp
# End Source File
# Begin Source File

SOURCE=..\src\luaconf.h
# End Source File
# Begin Source File

SOURCE=..\src\luajit.c
# End Source File
# Begin Source File

SOURCE=..\src\luajit.h
# End Source File
# Begin Source File

SOURCE=..\src\lualib.h
# End Source File
# End Target
# End Project
