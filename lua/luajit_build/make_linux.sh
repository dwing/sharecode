#!/bin/sh

cd `dirname $0`

cd ../src

GCCEXE=gcc

LJCOMPILE="$GCCEXE -pipe -O3 -fweb -fomit-frame-pointer -fmerge-all-constants -lm -s -Wall -Wextra -DNDEBUG -DLUAJIT_ENABLE_LUA52COMPAT -DLUAJIT_ENABLE_GC64 -DLUAJIT_TARGET=LUAJIT_ARCH_X64 -I. -I../dynasm"
ALL_LIB="lib_base.c lib_math.c lib_bit.c lib_string.c lib_table.c lib_io.c lib_os.c lib_package.c lib_debug.c lib_jit.c lib_ffi.c"

$LJCOMPILE -o minilua host/minilua.c

./minilua ../dynasm/dynasm.lua -LN -D JIT -D FFI -D P64 -o host/buildvm_arch.h vm_x64.dasc

git show -s --format=%ct >luajit_relver.txt
./minilua host/genversion.lua

$LJCOMPILE -m64 -o buildvm host/buildvm.c host/buildvm_asm.c host/buildvm_fold.c host/buildvm_lib.c host/buildvm_peobj.c

./buildvm -m elfasm  -o lj_vm.o
./buildvm -m bcdef   -o lj_bcdef.h    $ALL_LIB
./buildvm -m ffdef   -o lj_ffdef.h    $ALL_LIB
./buildvm -m libdef  -o lj_libdef.h   $ALL_LIB
./buildvm -m recdef  -o lj_recdef.h   $ALL_LIB
./buildvm -m vmdef   -o jit/vmdef.lua $ALL_LIB
./buildvm -m folddef -o lj_folddef.h lj_opt_fold.c

$LJCOMPILE -m64 -fPIC -shared -o libluajit64.so ljamalg.c lj_vm.o

$LJCOMPILE -m64 -o luajit64 ljamalg.c luajit.c lj_vm.o

#rm buildvm*.o minilua buildvm
