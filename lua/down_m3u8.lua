-- https://v.cdnlz19.com/20240311/20786_d9a2db19/2000k/hls/mixed.m3u8
-- https://yzzy.play-cdn19.com/20240322/24445_5ba58e8d/2000k/hls/mixed.m3u8
-- https://vidhub.in/watch/u84cm92/1.html
-- https://www.taijuz.cc/play/6nwqvm1/1.html

os.execute('curl -k -L -A "Mozilla/5.0" -o all.m3u8 "' .. arg[1] .. '"', "rb")
local f = io.open("all.m3u8", "rb")
local s = f:read "*a"
f:close()

local fns = {}
for line in s:gmatch "[^\r\n]+" do
	if line:match "^%w+%.ts$" then
		local fn = line:match "([^/]+)$"
		fns[#fns + 1] = "file '" .. fn .. "'\r\n"
	end
end
f = io.open("all.txt", "wb")
f:write(table.concat(fns))
f:close()

local path = arg[1]:match "^(.+/)[^/]*$"
local i = 0
for line in s:gmatch "[^\r\n]+" do
	if line:match "^%w+%.ts$" then
		local fn = line:match "([^/]+)$"
		i = i + 1
		print(path .. line .. " " .. i .. "/" .. #fns)
		f = io.open(fn, "rb")
		if f then
			f:close()
		else
			os.execute('curl -k -L -A "Mozilla/5.0" -o "' .. fn .. '" "' .. path .. line .. '"')
		end
	end
end

print "done! ffmpeg.exe -f concat -i all.txt -c copy all.mp4"
