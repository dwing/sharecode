-- luajit openai.lua "input"
-- luajit openai.lua [prompt.lua] input.txt output.txt
-- llama-server -m qwen2.5-7b-instruct-q5_k_m.gguf -c 4096 -ngl 100 --port 8080
-- curl -X POST -H "Content-Type: application/json" -T openai_upload.json -v "http://127.0.0.1:8080/v1/chat/completions"
--              -H "Authorization: Bearer $OPENAI_API_KEY"
--[[
{"model":"gpt-4o","stream":false,"cache_prompt":true,"temperature":0.1,"top_k":10,"top_p":0.1,"min_p":0.05,"max_tokens":1000,"messages":
[{"role":"system",   "content":"You are a helpful assistant."}
,{"role":"user",     "content":"你好"}
,{"role":"assistant","content":"你好！很高兴见到你。你可以问我任何问题，我会尽力帮助你。"}
,{"role":"user",     "content":"再见"}
]}
{"choices":[{"index":0,"finish_reason":"stop","message":{"role":"assistant","content":"再见！希望我们将来还有机会交流。祝你有一个美好的一天！"}}],
"usage":{"prompt_tokens":47,"completion_tokens":16,"total_tokens":63},
"timings":{"prompt_n":1,"prompt_ms":229.333,"prompt_per_token_ms":229.333,"prompt_per_second":4.36047145417363,"predicted_n":16,"predicted_ms":503.99,"predicted_per_token_ms":31.499375,"predicted_per_second":31.7466616401119},
"id":"chatcmpl-ipbhDUI79TqWNDlCp1NZkag8sfKuLEgt","object":"chat.completion","created":1740976033,"model":"gpt-3.5-turbo","system_fingerprint":"b4793-70680c48"}
--]]
------------------------------------------------------------------------------
local G = _G
G.url = 'http://127.0.0.1:8080/v1/chat/completions' -- default: 'http://127.0.0.1:8080/v1/chat/completions'
G.openai_api_key = nil -- default: nil
G.model = nil -- default: nil
G.temperature = 0.8 -- default: 0.8, disabled: 1
G.top_k = 40 -- default: 40, disabled: 0
G.top_p = 0.95 -- default: 0.95, disabled: 1
G.min_p = 0.05 -- default: 0.05, disabled: 0
G.max_tokens = -1 -- default: -1, disabled: -1
G.seed = nil -- default: nil
G.prompt = 'You are a helpful assistant.' -- default: 'You are a helpful assistant.'
G.debug = nil -- default: nil
------------------------------------------------------------------------------
local ffi = require 'ffi'
ffi.cdef[[
int __stdcall MultiByteToWideChar(int cp, int flag, const char* src, int srclen, char* wdst, int wdstlen);
int __stdcall WideCharToMultiByte(int cp, int flag, const char* src, int srclen, char* dst, int dstlen, const char* defchar, int* used);
]]
local function mb2wc(src, cp)
	local srclen = #src
	local dst = ffi.typeof 'char[?]'(srclen * 2)
	return ffi.string(dst, ffi.C.MultiByteToWideChar(cp or 65001, 0, src, srclen, dst, srclen) * 2)
end
local function wc2mb(src, cp)
	local srclen = #src / 2
	local dstlen = srclen * 3
	local dst = ffi.typeof 'char[?]'(dstlen)
	return ffi.string(dst, ffi.C.WideCharToMultiByte(cp or 65001, 0, src, srclen, dst, dstlen, nil, nil))
end
local function utf8_local(s)
	return wc2mb(mb2wc(s), 1)
end
local function local_utf8(s)
	return wc2mb(mb2wc(s, 1))
end
------------------------------------------------------------------------------
local function escapeCmd(cmd)
	return cmd:gsub('(\\+)"', function(s) return s .. s .. '"' end):gsub('(\\+)$', function(s) return s .. s end):gsub('"', '\\"')
end

local function genCmdHeader()
	return 'curl "' .. G.url .. '" -X POST -H "Content-Type: application/json"' .. (G.openai_api_key and (' -H "Authorization: Bearer ' .. G.openai_api_key .. '"') or '')
end

local function escapeJsonWithQuot(str)
	return string.format('%q', str or ''):gsub('\\\n', '\\n')
end

local tonumber = tonumber
local function genJsonHeader(noSpace)
	return table.concat{
		'{', G.model and ('"model":' .. escapeJsonWithQuot(G.model) .. ',') or '',
		'"stream":false,"cache_prompt":true',
		G.temperature and (',"temperature":' .. tonumber(G.temperature)) or '',
		G.top_k and (',"top_k":' .. tonumber(G.top_k)) or '',
		G.top_p and (',"top_p":' .. tonumber(G.top_p)) or '',
		G.min_p and (',"min_p":' .. tonumber(G.min_p)) or '',
		G.max_tokens and (',"max_tokens":' .. tonumber(G.max_tokens)) or '',
		G.seed and (',"seed":' .. tonumber(G.seed)) or '',
		noSpace and ',"messages":[{"role":"system","content":' or ',"messages":\n[{"role":"system",   "content":',
		escapeJsonWithQuot(G.prompt or 'You are a helpful assistant.'), noSpace and '}' or '}\n'
	}
end

local io = io
local print = print
local function callAiCmd(cmd)
	if G.debug then
		print('cmd: ' .. utf8_local(cmd))
	else
		cmd = cmd .. ' 2>nul'
	end
	local f = io.popen(utf8_local(cmd), 'rb')
	local res = f:read '*a'
	f:close()
	local _, q = res:find '"content"%s*:%s*"'
	if not q then
		return nil, cmd, res
	end
	if G.debug then
		print('res: ' .. utf8_local(res))
	end
	local p = q
	local s = res
	while q and q < #s do
		local b = s:byte(q + 1)
		if b == 0x22 then -- '"'
			s = s:sub(p + 1, q)
			break
		end
		q = q + (b == 0x5c and 2 or 1) -- '\'
	end
	return (s:gsub('\\(.)', function(c)
			if c == 'b' then return '\b'
		elseif c == 'f' then return '\f'
		elseif c == 'n' then return '\n'
		elseif c == 'r' then return '\r'
		elseif c == 't' then return '\t'
		-- elseif c == 'u' then TODO
		end
	end)), cmd, res
end

G.callAiFile = function(jsonFileName)
	return callAiCmd(genCmdHeader() .. ' -T "' .. jsonFileName .. '"')
end

G.callAiStr = function(str)
	local json = genJsonHeader(true) .. ',{"role":"user","content":' .. escapeJsonWithQuot(str) .. '}]}'
	return callAiCmd(genCmdHeader() .. ' -d "' .. escapeCmd(json) .. '"')
end
------------------------------------------------------------------------------
local error = error
local tostring = tostring
local promptFileName, inputFileName, outputFileName
local args = {...}
if #args == 3 then
	promptFileName, inputFileName, outputFileName = args[1], args[2], args[3]
elseif #args == 2 then
	inputFileName, outputFileName = args[1], args[2]
elseif #args == 1 then
	local line = args[1]
	print('> ' .. line)
	local lastResult, cmd, res = G.callAiStr(local_utf8(line))
	if lastResult then
		print('< ' .. utf8_local(lastResult))
	else
		error('ERROR: callAiStr failed:\ncmd: ' .. utf8_local(tostring(cmd)) .. '\nres: ' .. utf8_local(tostring(res)))
	end
	return
else
	return
end

if promptFileName then
	dofile(promptFileName)
end

local tmpFileName = G.tmpFileName or 'openai.tmp.json'
local tmpFileNameLocal = utf8_local(tmpFileName)
local ftmp = io.open(tmpFileNameLocal, 'wb')
if not ftmp then error('ERROR: can not create ' .. tmpFileNameLocal) end
ftmp:write(genJsonHeader(), ']}\n')
ftmp:close()

local fout = io.open(outputFileName, 'wb')
if not fout then error('ERROR: can not create ' .. outputFileName) end

local lastResult, cmd, res
for line in io.lines(inputFileName) do
	line = line:gsub('^%s+', ''):gsub('%s+$', '') -- trim
	if line ~= '' then
		print('> ' .. utf8_local(line))
		ftmp = io.open(tmpFileNameLocal, 'rb+')
		if not ftmp then error('ERROR: can not open ' .. tmpFileNameLocal) end
		ftmp:seek('end', -3)
		if lastResult then
			ftmp:write(',{"role":"assistant","content":' .. escapeJsonWithQuot(lastResult) .. '}\n')
		end
			ftmp:write(',{"role":"user",     "content":' .. escapeJsonWithQuot(line) .. '}\n]}\n')
		ftmp:close()
		lastResult, cmd, res = G.callAiFile(tmpFileName)
		if not lastResult then
			error('ERROR: callAiFile(' .. outputFileName .. ') failed:\ncmd: ' .. utf8_local(tostring(cmd)) .. '\nres: ' .. utf8_local(tostring(res)))
		end
		line = lastResult
		line = line:gsub('^<think>.-</think>%s*', ''):gsub('[\r\n]+', '')
		print('< ' .. utf8_local(line))
	end
	fout:write(line, '\n')
	fout:flush()
end
fout:close()
print 'DONE!'
if lastResult then
	ftmp = io.open(tmpFileNameLocal, 'rb+')
	if not ftmp then error('ERROR: can not open ' .. tmpFileNameLocal) end
	ftmp:seek('end', -3)
	ftmp:write(',{"role":"assistant","content":' .. escapeJsonWithQuot(lastResult) .. '}\n]}\n')
	ftmp:close()
end
