local f = io.open("openmw.exe", "rb")
local s = f:read "*a"
f:close()

-- %USERPROFILE%\Documents\My Games\OpenMW settings.cfg : fallback=Fonts_Font_0,DejaVuLGCSansMono
-- DejaVuLGCSansMono.omwfont : <Property key="Source" value="msyh.ttc"/>
--                                 <Code range="33 65535"/>

-- tables_gen.hpp : windows_1252
local p = s:find("\x02\xc3\xbe\x00\x00\x00\x02\xc3\xbf\x00\x00\x00", 1, true)
if p then
	f = io.open("openmw-cn.exe", "wb")
	f:write(s:sub(1, p + 12 - 0x300 - 1))
	for i = 0x80, 0xff do
		f:write("\x01")
		f:write(string.char(i))
		f:write("\x00\x00\x00\x00")
	end
	f:write(s:sub(p + 12, -1))
	f:close()
	print "OK"
else
	print "NOT FOUND"
end

-- TODO: bookpage.cpp : writeImpl : ucsBreakingSpace for wide char
