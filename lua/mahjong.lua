-- 玩家VS电脑二人麻将,最简单的规则(不支持暗杠,电脑AI有些行为不够智能)

local write = io.write
local sort = table.sort
local remove = table.remove

local PLAYER_COUNT = 2
local PLAYER_TILE_COUNT = 13

local ALL_TILES = -- (9*3+7)*4 = 136
{
	 1,  2,  3,  4,  5,  6,  7,  8,  9, -- 万
	11, 12, 13, 14, 15, 16, 17, 18, 19, -- 筒
	21, 22, 23, 24, 25, 26, 27, 28, 29, -- 条
	31, 32, 33, 34,     36, 37, 38,     -- 东南西北 中发白 (不可吃)
--	41, 42, 43, 44,     46, 47, 48, 49, -- 春夏秋冬 梅兰竹菊 (不可吃)
}
local NUM_TILES = #ALL_TILES
for i = NUM_TILES + 1, NUM_TILES * 4 do
	ALL_TILES[i] = ALL_TILES[i - NUM_TILES]
end
NUM_TILES = #ALL_TILES

math.randomseed(os.time())
local function shuffleAllTiles()
	local tiles = {}
	for i = 1, NUM_TILES do
		tiles[i] = ALL_TILES[i]
	end
	for i = 1, NUM_TILES do
		local j = math.random(i, NUM_TILES)
		local t = tiles[i]; tiles[i] = tiles[j]; tiles[j] = t
	end
	return tiles
end

local function isTileSameType(a, b)
	if a < 10 then return b < 10 end
	if a < 20 then return b < 20 end
	if a < 30 then return b < 30 end
	if a < 35 then return b < 35 end
	return true
end

local function findTile(tiles, v)
	for i = 1, #tiles do
		if tiles[i] == v then return i end
	end
end

local function findTileRev(tiles, v)
	for i = #tiles, 1, -1 do
		if tiles[i] == v then return i end
	end
end

local function findTilePrev(tiles, i, v)
	for i = i, 1, -1 do
		local a = tiles[i]
		if a == v then return i end
		if a < v then return end
	end
end

local function findTileNext(tiles, i, v)
	for i = i, #tiles do
		local a = tiles[i]
		if a == v then return i end
		if a > v then return end
	end
end

local function findTileCount(tiles, i)
	local n, v = 1, tiles[i]
	for j = i + 1, #tiles do
		if tiles[j] ~= v then break end
		n = n + 1
	end
	for j = i - 1, 1, -1 do
		if tiles[j] ~= v then break end
		n = n + 1
	end
	return n
end

local function clone(t)
	local r = {}
	for i = 1, #t do
		r[i] = t[i]
	end
	return r
end

local TILE_NAME_FIRST1 = { "一", "二", "三", "四", "五", "六", "七", "八", "九" }
local TILE_NAME_FIRST2 = { "东", "南", "西", "北", "",   "中", "发", "白" }
local function getFirstStr(t)
	return (t < 30 and TILE_NAME_FIRST1 or TILE_NAME_FIRST2)[t % 10]
end
local function getSecondStr(t)
	if t < 10 then return "万" end
	if t < 20 then return "筒" end
	if t < 30 then return "条" end
	if t < 35 then return "风" end
	return "　"
end

local gameState =
{
	-- allTiles = {},
	-- tilePos = 0, -- 最近取牌的allTiles下标
	-- poolTiles = {},
	-- playerTiles = {},
	-- showTiles = {},
	-- nowPlayerId = 1,
	-- nowTile = 0,
}

function gameState:reset()
	self.allTiles = shuffleAllTiles()
	self.tilePos = 0
	self.poolTiles = {}
	self.playerTiles = {}
	self.showTiles = {}
	for i = 1, PLAYER_COUNT do
		local tiles = {}
		self.playerTiles[i] = tiles
		self.showTiles[i] = {}
		for j = 1, PLAYER_TILE_COUNT do
			tiles[j] = self:draw()
		end
	end
	for i = 1, PLAYER_COUNT do
		sort(self.playerTiles[i])
	end
	self.nowPlayerId = 1
	self.nowTile = 0
end

function gameState:draw() -- 摸牌
	local p = self.tilePos + 1
	self.tilePos = p
	local t = self.allTiles[p]
	return t
end

function gameState:mergeAndDiscard(id, i) -- 出牌
	local pool = self.poolTiles
	local tiles = self.playerTiles[id]
	if i == 0 then
		i = #tiles + 1
	end
	if id == self.nowPlayerId and self.nowTile > 0 then
		tiles[#tiles + 1] = self.nowTile
		self.nowTile = 0
	end
	local t = tiles[i]
	if not t then return end
	remove(tiles, i)
	sort(tiles)
	pool[#pool + 1] = t
	return true
end

function gameState:canChow(id) -- 能否吃
	local tiles = self.playerTiles[id]
	local t = self.poolTiles[#self.poolTiles]
	if t > 30 then return end
	local r = {}
	local i = findTileRev(tiles, t - 2)
	if i and findTile(tiles, t - 1) then
		r[#r + 1] = i
	end
	i = findTileRev(tiles, t - 1)
	if i and findTile(tiles, t + 1) then
		r[#r + 1] = i
	end
	if findTile(tiles, t + 1) and findTile(tiles, t + 2) then
		r[#r + 1] = findTile(tiles, t) or findTile(tiles, t + 1)
	end
	if #r == 1 then r[1] = 0 end -- 可以省略参数
	return #r > 0 and r
end

function gameState:canPong(id) -- 能否碰
	local tiles = self.playerTiles[id]
	local t = self.poolTiles[#self.poolTiles]
	local n = 0
	for i = 1, #tiles do
		if tiles[i] == t then n = n + 1 end
	end
	return n >= 2
end

function gameState:canKong(id) -- 能否杠
	local tiles = self.playerTiles[id]
	local t = self.poolTiles[#self.poolTiles]
	local n = 0
	for i = 1, #tiles do
		if tiles[i] == t then n = n + 1 end
	end
	return n >= 3
end

local function markChow(t, i)
	local a = t[i]
	if a > 30 then return end
	local j = findTileNext(t, i + 1, a + 1); if not j then return end
	local k = findTileNext(t, j + 1, a + 2); if not k then return end
	return j, k
end

local function isPair(t, i)
	return i + 1 <= #t and t[i] == t[i + 1]
end

local function isPong(t, i)
	return i + 2 <= #t and t[i] == t[i + 1] and t[i] == t[i + 2]
end

local function isKong(t, i)
	return i + 3 <= #t and t[i] == t[i + 1] and t[i] == t[i + 2] and t[i] == t[i + 3]
end

local function isResultWin(r)
	return r[2] == 1
end

local function matchTiles(tiles, i, r)
	while tiles[i] == 0 do i = i + 1 end
	if i > #tiles then return isResultWin(r) end
	local j, k = markChow(tiles, i)
	if j then
		local a = tiles[i]
		tiles[j] = 0; tiles[k] = 0
		r[1] = r[1] + 1
		if matchTiles(tiles, i + 1, r) then return true end
		r[1] = r[1] - 1
		tiles[j] = a + 1; tiles[k] = a + 2
	end
	if isPair(tiles, i) then r[2] = r[2] + 1; if matchTiles(tiles, i + 2, r) then return true end; r[2] = r[2] - 1 end
	if isPong(tiles, i) then r[3] = r[3] + 1; if matchTiles(tiles, i + 3, r) then return true end; r[3] = r[3] - 1 end
	if isKong(tiles, i) then r[4] = r[4] + 1; if matchTiles(tiles, i + 4, r) then return true end; r[4] = r[4] - 1 end
end

function gameState:canWin(id) -- 能否胡
	local tiles = self.playerTiles[id]
	local t = self.nowPlayerId == id and self.nowTile or 0
	if t <= 0 then t = self.poolTiles[#self.poolTiles] end
	tiles = clone(tiles)
	tiles[#tiles + 1] = t
	sort(tiles)
	local result = {0, 0, 0, 0}
	return matchTiles(tiles, 1, result)
end

function gameState:doChow(id, i) -- 吃
	local tiles = self.playerTiles[id]
	local t = self.poolTiles[#self.poolTiles]
	local t1 = i and tiles[i] or t
	if not t1 or t > 30 then return end
	local a, b, c, d = findTile(tiles, t - 2), findTile(tiles, t - 1), findTile(tiles, t + 1), findTile(tiles, t + 2)
		if c and d and t1 >= t     then t1 = t
	elseif b and c and t1 >= t - 1 then t1 = t - 1
	elseif a and b and t1 >= t - 2 then t1 = t - 2 end
	tiles[#tiles + 1] = t
	remove(tiles, findTileRev(tiles, t1))
	remove(tiles, findTileRev(tiles, t1 + 1))
	remove(tiles, findTileRev(tiles, t1 + 2))
	self.poolTiles[#self.poolTiles] = nil
	local shows = self.showTiles[id]
	shows[#shows + 1] = {t1, t1 + 1, t1 + 2}
	return true
end

function gameState:doPong(id) -- 碰
	local tiles = self.playerTiles[id]
	local t = self.poolTiles[#self.poolTiles]
	for i = #tiles - 1, 1, -1 do
		if tiles[i] == t and tiles[i + 1] == t then
			remove(tiles, i)
			remove(tiles, i)
			self.poolTiles[#self.poolTiles] = nil
			local shows = self.showTiles[id]
			shows[#shows + 1] = {t, t, t}
			return true
		end
	end
end

function gameState:doKong(id) -- 杠
	local tiles = self.playerTiles[id]
	local t = self.poolTiles[#self.poolTiles]
	for i = #tiles - 2, 1, -1 do
		if tiles[i] == t and tiles[i + 1] == t and tiles[i + 2] == t then
			remove(tiles, i)
			remove(tiles, i)
			remove(tiles, i)
			self.poolTiles[#self.poolTiles] = nil
			local shows = self.showTiles[id]
			shows[#shows + 1] = {t, t, t, t}
			return true
		end
	end
end

local INDENT = "　　　　"
local TILE_CHARS = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "q", "w", "e" }
function gameState:printPlayerTiles(id, withNumber)
	local tiles = self.playerTiles[id]
	local n = #tiles
	local last = 0
	write(INDENT)
	for i = 1, n do
		local t = tiles[i]
		if last > 0 and not isTileSameType(last, t) then write("　") end
		write(getFirstStr(t))
		last = t
	end
	if id == self.nowPlayerId and self.nowTile > 0 then
		write("｜")
		write(getFirstStr(self.nowTile))
	end
	write("\n", INDENT)

	last = 0
	for i = 1, n do
		local t = tiles[i]
		if last > 0 and not isTileSameType(last, t) then write("　") end
		write(getSecondStr(t))
		last = t
	end
	if id == self.nowPlayerId and self.nowTile > 0 then
		write("｜")
		write(getSecondStr(self.nowTile))
	end
	write("\n")

	if withNumber then
		last = 0
		write(INDENT)
		for i = 1, n do
			local t = tiles[i]
			if last > 0 and not isTileSameType(last, t) then write("　") end
			write(" ", TILE_CHARS[i])
			last = t
		end
		if id == self.nowPlayerId and self.nowTile > 0 then
			write("｜")
		end
		write("\n")
	end
end

function gameState:printShowTiles(id)
	local tiless = self.showTiles[id]
	write(INDENT)
	for i = 1, #tiless do
		local tiles = tiless[i]
		for j = 1, #tiles do write(getFirstStr(tiles[j])) end
		write("　")
	end
	write("\n", INDENT)
	for i = 1, #tiless do
		local tiles = tiless[i]
		for j = 1, #tiles do write(getSecondStr(tiles[j])) end
		write("　")
	end
	write("\n")
end

function gameState:printPoolTiles(i, j)
	local tiles = self.poolTiles
	for k = i, j do write(getFirstStr(tiles[k])) end
	write("\n")
	for k = i, j do write(getSecondStr(tiles[k])) end
	write("\n")
end

local POOL_LINE_COUNT = 4
local POOL_LINE_TILE_COUNT = 27
function gameState:printAllPoolTiles()
	local tiles = self.poolTiles
	local n = #tiles
	for k = 1, POOL_LINE_COUNT do
		local i = (k - 1) * POOL_LINE_TILE_COUNT + 1
		local j = i + POOL_LINE_TILE_COUNT - 1
		self:printPoolTiles(i, j < n and j or n)
		write("\n")
	end
end

function gameState:printAll(discard)
	os.execute("cls")
	write(INDENT, "———————————————————\n")
	self:printPlayerTiles(2)
	write(INDENT, "…………………………………………………\n")
	self:printShowTiles(2)
	write(INDENT, "———————————————————\n")
	self:printAllPoolTiles()
	write(INDENT, "———————————————————\n")
	self:printShowTiles(1)
	write(INDENT, "…………………………………………………\n")
	self:printPlayerTiles(1, true)
	write(INDENT, "———————————————————\n")
	if self.nowPlayerId == 1 then
		if not discard then
			write(INDENT, "输入(回车取牌")
			local r = self:canChow(1)
			if r then
				for i = 1, #r do
					local id = r[i]
					write(string.format(", c%s吃", id > 0 and TILE_CHARS[id] or ""))
				end
			end
			if self:canPong(1) then write(", p碰") end
			if self:canKong(1) then write(", g杠") end
			if self:canWin (1) then write(", h和") end
			write("): ")
		else
			write(INDENT, self.nowTile > 0 and "输入(牌下字符,回车出牌" or "输入(牌下字符出牌")
			if self:canWin (1) then write(", h和") end -- 自摸
			write("): ")
		end
		return io.read()
	end
end

local function charToId(c)
	for i = 1, #TILE_CHARS do
		if TILE_CHARS[i] == c then
			return i
		end
	end
end

function gameState:start()
	os.execute("mode con cols=55 lines=30")
--	os.execute("mode con cols=55 lines=57")
	gameState:reset()
	local round = 0
	while true do
		round = round + 1
		self.nowPlayerId = 1
		if round > 1 then
			self.nowTile = 0
			local cmd = self:printAll()
			if cmd:sub(1, 1) == "c" then
				self:doChow(1, charToId(cmd:sub(2)))
			elseif cmd == "p" then
				self:doPong(1)
			elseif cmd == "g" then
				self:doKong(1)
				self.nowTile = self:draw()
				if not self.nowTile then break end
			elseif cmd == "h" then
				if self:canWin(1) then
					write("玩家和牌!\n")
					return
				end
			else
				self.nowTile = self:draw()
				if not self.nowTile then break end
			end
		else
			self.nowTile = self:draw()
		end

		repeat
			local cmd = self:printAll(true)
			if cmd == "h" then
				if self:canWin(1) then
					write("玩家和牌!\n")
					return
				end
			end
		until self:mergeAndDiscard(1, charToId(cmd) or 0)

		self.nowPlayerId = 2
		if not self:doAI(2) then break end
	end
end

local function scoreTile(tiles, i)
	local t = tiles[i]
	if t < 30 then
		local n = findTileCount(tiles, i) * 4
		if findTilePrev(tiles, i, t - 2) then n = n + 1 end
		if findTilePrev(tiles, i, t - 1) then n = n + 3 end
		if findTileNext(tiles, i, t + 1) then n = n + 3 end
		if findTileNext(tiles, i, t + 2) then n = n + 1 end
		return n
	else
		return findTileCount(tiles, i) * 3
	end
end

local function scoreTiles(tiles, i, r, v)
	v = v or 0
	while tiles[i] == 0 do i = i + 1 end
	if i > #tiles then
		if v > r[1] then r[1] = v end
		return
	end
	local j, k = markChow(tiles, i)
	if j then
		tiles[j] = 0; tiles[k] = 0
		scoreTiles(tiles, i + 1, r, v + 100)
		local a = tiles[i]; tiles[j] = a + 1; tiles[k] = a + 2
	end
	if isPair(tiles, i) then scoreTiles(tiles, i + 2, r, v +  50) end
	if isPong(tiles, i) then scoreTiles(tiles, i + 3, r, v + 150) end
--	if isKong(tiles, i) then scoreTiles(tiles, i + 4, r, v + 200) end
							 scoreTiles(tiles, i + 1, r, v + scoreTile(tiles, i))
end

function gameState:doAIDiscard(id)
	if self:canWin(id) then
		write(string.format("电脑和牌(自摸%s%s)!\n", getFirstStr(self.nowTile), getSecondStr(self.nowTile)))
		return
	end
	local tiles = clone(self.playerTiles[id])
	if self.nowPlayerId == id and self.nowTile > 0 then
		tiles[#tiles + 1] = self.nowTile
		sort(tiles)
	end
	local last, bestScore, bestTile = nil, 0, 0
	for i = 1, #tiles do
		local t = tiles[i]
		if t ~= last then
			remove(tiles, i)
			local score = {0}
			scoreTiles(tiles, 1, score)
			table.insert(tiles, i, t)
			if bestScore < score[1] then
				bestScore = score[1]
				bestTile = t
			end
			last = t
		end
	end
	return bestTile ~= self.nowTile and findTileRev(self.playerTiles[id], bestTile) or 0
end

function gameState:doAI(id)
	if self:canWin(id) then
		self:printAll()
		write("电脑和牌!\n")
		return
	end
	local needDraw = true
		if self:canKong(id) then
		self:doKong(id)
	elseif self:canPong(id) then
		self:doPong(id)
		needDraw = false
	elseif self:canChow(id) then
		self:doChow(id)
		needDraw = false
	end
	if needDraw then
		self.nowTile = self:draw()
		if not self.nowTile then return end
	end
	local i = self:doAIDiscard(id)
	if not i then return end
	self:mergeAndDiscard(id, i)
	return true
end

gameState:start()

-- @echo off
-- setlocal
-- pushd %~dp0
--
-- luajit mahjong.lua
--
-- pause
